<?php

include ("Net/IPv6.php");


function show_example($obj, $arr)
{
	foreach ($arr AS $key=>$val)
	{
		list ($param, $rettype) = $val;

		print "\n";
		print "<b>\$netipv6->{$key}($param)</b>\n";
		print "--------------------------------------------------------------------------\n";
		($rettype == "string") ? print $obj->{$key}($param) : false;
		($rettype == "array") ? var_dump($obj->{$key}($param)) : false;
		print "\n";
		print "\n";
	}
}

//
// Engine
//
$netipv6 = new Net_IPv6();

$ip = "fe80::21b:eedf:0:eff2";
$pl = "64";

(array) $arr_examples = array(
	"separate" => array("$ip/$pl", "array"),
	"removePrefixLength" => array("$ip/$pl", "string"),
	"getNetmaskSpec" => array("$ip/$pl", "string"),
	"getPrefixLength" => array("$ip/$pl", "string"),
	"getNetmask" => array("$ip/$pl", "string"),
	//"isInNetmask" => array(),
	"getAddressType" => array("$ip", "string"),
	"uncompress" => array("$ip", "string"),
	"compress" => array("$ip", "string"),
	"recommendedFormat" => array("$ip", "string"),
	//"isCompressible" => array("$ip", "string"),
	"SplitV64" => array("$ip/$pl", "array"),
	"checkIPv6" => array("$ip", "string"),
	"parseAddress" => array("$ip/$pl", "array"),
	"_ip2Bin" => array("$ip", "string"),
	"_bin2Ip" => array("$ip", "string"),
);


print "<pre>\n";
show_example($netipv6, $arr_examples);
print "</pre>\n";

?>
